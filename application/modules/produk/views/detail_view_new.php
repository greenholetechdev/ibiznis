<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tipe Produk
     </div>
     <div class='col-md-3'>
      <?php echo $tipe ?>
     </div>     

     <div class='col-md-3 text-bold'>
      Produk Kategori
     </div>
     <div class='col-md-3'>
      <?php echo $product_kategori ?>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3 text-bold'>
      Nama Produk
     </div>
     <div class='col-md-3'>
      <?php echo $product ?>
     </div>

     <div class='col-md-3 text-bold'>
      Kode Product
     </div>
     <div class='col-md-3'>
      <?php echo $kode_product ?>
     </div>
    </div>
    <br/>
   
    <div class='row'>
     <div class='col-md-3 text-bold'>
      Keterangan Produk
     </div>
     <div class='col-md-3'>
      <?php $keterangan ?>
     </div>
    </div>
    <br/>

    <hr/>

    <div class='row'>
     <div class='col-md-3 text-bold'>
      <u>Foto Produk</u>
     </div>
    </div>
    <br/>    

    <div class='row'>
     <?php foreach ($data_image as $v_image) { ?>
      <div class='col-md-4'>
       <div class=''>
        <img src="<?php echo $v_image['foto'] ?>" width="150" height="150"/>
       </div>       
      </div>
     <?php } ?>
    </div>
    <br/>
    <hr/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="Produk.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
