<div class="table-responsive">
 <table class="table table-striped table-bordered table-list-draft">
  <thead>
   <tr class="bg-primary-light text-white">
    <th>Bank</th>
    <th>No Rekening</th>
    <th>Akun</th>
    <th class="text-center">Action</th>
   </tr>
  </thead>
  <tbody>
   <?php if (!empty($list_bank)) { ?>
    <?php foreach ($list_bank as $value) { ?>
     <tr data_id="<?php echo $value['id'] ?>">
      <td><?php echo $value['nama_bank'] ?></td>
      <td><?php echo $value['no_rekening'] ?></td>
      <td><?php echo $value['akun'] ?></td>
      <td class="text-center">
       <button class="btn btn-success" index_row="<?php echo $index ?>" 
               onclick="FakturPelanggan.pilihBank(this)">Pilih</button>
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
  </tbody>
 </table>
</div>