<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Pajak</u>
     </div>
    </div> 
    <hr/>

    <div class="row">
     <div class='col-md-3'>
      Jenis Pajak
     </div>
     <div class='col-md-3'>
      <input type='text' name='' placeholder="" id='jenis' class='form-control required' 
             value='<?php echo isset($jenis) ? $jenis : '' ?>' error="Jenis Pajak"/>
     </div>     
    </div>
    <br/>
   
    <div class="row">
     <div class='col-md-3'>
      Persentase (%)
     </div>
     <div class='col-md-3'>
      <input type='number' name='' placeholder="" id='persentase' class='form-control required' 
             value='<?php echo isset($persentase) ? $persentase : '0' ?>' 
             error="Persentase" min="0" />
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <textarea id="keterangan" class="form-control required" error="Keterangan"><?php echo isset($keterangan) ? $keterangan: '' ?></textarea>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="Pajak.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Pajak.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
