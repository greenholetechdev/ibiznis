<?php

class Pengguna extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'pengguna';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/pengguna.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'user';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pengguna";
  $data['title_content'] = 'Data Pengguna';
  $content = $this->getDataPengguna();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPengguna($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('i.username', $keyword),
       array('i.password', $keyword),
       array('pg.nama_pegawai', $keyword),
       array('p.hak_akses', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' i',
              'field' => array('i.*', 'p.hak_akses', 'pg.nama as nama_pegawai'),
              'join' => array(
                  array('priveledge p', 'i.priveledge = p.id'),
                  array('pegawai pg', 'pg.id = i.pegawai', 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "i.deleted is null or i.deleted = 0"
  ));

  return $total;
 }

 public function getDataPengguna($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('i.username', $keyword),
       array('i.password', $keyword),
       array('pg.nama_pegawai', $keyword),
       array('p.hak_akses', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' i',
              'field' => array('i.*', 'p.hak_akses', 'pg.nama as nama_pegawai'),
              'join' => array(
                  array('priveledge p', 'i.priveledge = p.id'),
                  array('pegawai pg', 'pg.id = i.pegawai', 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "i.deleted is null or i.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPengguna($keyword)
  );
 }

 public function getDetailDataPengguna($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => array('kr.*', 'p.hak_akses'),
              'join' => array(
                  array('priveledge p', 'kr.priveledge = p.id')
              ),
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListPriveledge() {
  $data = Modules::run('database/get', array(
              'table' => 'priveledge',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pengguna";
  $data['title_content'] = 'Tambah Pengguna';
  $data['list_akses'] = $this->getListPriveledge();
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPengguna($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Pengguna";
  $data['title_content'] = 'Ubah Pengguna';
  $data['list_akses'] = $this->getListPriveledge();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataPengguna($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pengguna";
  $data['title_content'] = 'Detail Pengguna';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['username'] = $value->username;
  $data['password'] = md5($value->password);
  $data['priveledge'] = $value->priveledge;
  if ($value->pegawai != '-') {
   $data['pegawai'] = $value->pegawai;
  }
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;
  $pengguna = $id;
  $this->db->trans_begin();
  try {
   $post_user = $this->getPostDataHeader($data);
   if ($id == '') {
    $pengguna = Modules::run('database/_insert', $this->getTableName(), $post_user);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_user, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'pengguna' => $pengguna));
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', 'user', array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pengguna";
  $data['title_content'] = 'Data Pengguna';
  $content = $this->getDataPengguna($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

}
