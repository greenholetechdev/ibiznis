<?php

class Faktur_pelanggan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'faktur_pelanggan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/faktur_pelanggan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'invoice';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Faktur";
  $data['title_content'] = 'Data Faktur';
  $content = $this->getDataFaktur();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['akses'] = $this->akses;
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataFaktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('i.no_faktur', $keyword),
       array('i.tanggal_faktur', $keyword),
       array('i.tanggal_bayar', $keyword),
       array('pb.nama', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' i',
              'field' => array(
                  'i.*', 'isa.status',
                  'sisa.jumlah as sisa_hutang', 'isp.approve as approve_print', 'pb.nama as nama_pembeli'
              ),
              'join' => array(
                  array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
                  array('invoice_status isa', 'isa.id = iss.id'),
                  array('(select max(id) id, invoice from invoice_sisa group by invoice) issa', 'issa.invoice = i.id', 'left'),
                  array('invoice_sisa sisa', 'sisa.id = issa.id', 'left'),
                  array('(select max(id) id, invoice from invoice_print group by invoice) issp', 'issp.invoice = i.id', 'left'),
                  array('invoice_print isp', 'isp.id = issp.id', 'left'),
                  array('pembeli pb', 'pb.id = i.pembeli', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "i.deleted is null or i.deleted = 0",
              'orderby' => 'i.id desc'
  ));

  return $total;
 }

 public function getDataFaktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('i.no_faktur', $keyword),
       array('i.tanggal_faktur', $keyword),
       array('i.tanggal_bayar', $keyword),
       array('pb.nama', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' i',
              'field' => array(
                  'i.*', 'isa.status',
                  'sisa.jumlah as sisa_hutang', 'isp.approve as approve_print', 'pb.nama as nama_pembeli'
              ),
              'join' => array(
                  array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
                  array('invoice_status isa', 'isa.id = iss.id'),
                  array('(select max(id) id, invoice from invoice_sisa group by invoice) issa', 'issa.invoice = i.id', 'left'),
                  array('invoice_sisa sisa', 'sisa.id = issa.id', 'left'),
                  array('(select max(id) id, invoice from invoice_print group by invoice) issp', 'issp.invoice = i.id', 'left'),
                  array('invoice_print isp', 'isp.id = issp.id', 'left'),
                  array('pembeli pb', 'pb.id = i.pembeli', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "i.deleted is null or i.deleted = 0",
              'orderby' => 'i.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['sisa_hutang'] = $value['sisa_hutang'] == '' ? $value['total'] : $value['sisa_hutang'];
    $value['terbayar'] = $value['total'] - $value['sisa_hutang'];
    $value['sisa'] = $value['total'] - $value['terbayar'];
    //    if($value['no_faktur'] == 'INV19OCT001'){
    //     echo '<pre>';
    //     print_r($value);die;
    //    }
    array_push($result, $value);
   }
  }

  // echo '<pre>';
  // echo $this->db->last_query();die;
  //  echo '<pre>';
  //  print_r($result);die;
  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataFaktur($keyword)
  );
 }

 public function getDetailDataFaktur($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' i',
              'field' => array(
                  'i.*',
                  'p.nama as nama_pembeli', 'ist.status',
                  'o.no_order', 'p.alamat',
                  'pt.potongan as jenis_potongan',
                  'jp.jenis as jenis_bayar',
                  'pg.nama as sales'
              ),
              'join' => array(
                  array('pembeli p', 'i.pembeli = p.id'),
                  array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
                  array('invoice_status ist', 'ist.id = iss.id'),
                  array('`order` o', 'o.id = i.ref', 'left'),
                  array('jenis_pembayaran jp', 'jp.id = i.jenis_pembayaran', 'left'),
                  array('potongan pt', 'pt.id = i.potongan', 'left'),
                  array('user usr', 'usr.id = o.createdby', 'left'),
                  array('pegawai pg', 'pg.id = usr.pegawai', 'left'),
              ),
              'where' => "i.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListProduct() {
  $data = Modules::run('database/get', array(
              'table' => 'product_satuan ps',
              'field' => array('ps.*', 'p.product as nama_product', 's.nama_satuan', 'p.kode_product'),
              'join' => array(
                  array('product p', 'ps.product = p.id'),
                  array('satuan s', 'ps.satuan = s.id', 'left'),
              ),
              'where' => "ps.deleted = 0 or ps.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPelanggan() {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null and p.pembeli_kategori = 2"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPajak() {
  $data = Modules::run('database/get', array(
              'table' => 'pajak p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListMetodeBayar() {
  $data = Modules::run('database/get', array(
              'table' => 'metode_bayar',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListJenisBayar() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_pembayaran',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Faktur";
  $data['title_content'] = 'Tambah Faktur';
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_jenis_bayar'] = $this->getListJenisBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['list_potongan'] = $this->getListPotongan();
  //  echo '<pre>';
  //  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function bayar() {
  $data['view_file'] = 'form_bayar';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Bayar Faktur";
  $data['title_content'] = 'Bayar Faktur';
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  //  echo '<pre>';
  //  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataFaktur($id);
  //  echo $data['total'];die;
  //  echo '<pre>';
  //  print_r($data);die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Faktur";
  $data['title_content'] = 'Ubah Faktur';
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['invoice_item'] = $this->getListInvoiceItem($id);
  echo Modules::run('template', $data);
 }

 public function detail($id, $approve = "") {
  $data = $this->getDetailDataFaktur($id);
  //  echo '<pre>';
  //  print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Faktur";
  $data['title_content'] = 'Detail Faktur';
  $data['invoice_item'] = $this->getListInvoiceItem($id);
  $data['approve'] = $approve;
  $data['list_jenis_bayar'] = $this->getListJenisBayar();
  echo Modules::run('template', $data);
 }

 public function getListInvoiceItem($invoice) {
  $data = Modules::run('database/get', array(
              'table' => 'invoice_product ip',
              'field' => array(
                  'ip.*', 'ps.satuan', 'ps.harga',
                  'p.product as nama_product',
                  'p.kode_product',
                  'b.nama_bank', 'b.akun',
                  'b.no_rekening', 'pj.jenis',
                  'pj.persentase', 'm.metode',
                  's.nama_satuan'
              ),
              'join' => array(
                  array('product_satuan ps', 'ps.id = ip.product_satuan'),
                  array('product p', 'p.id = ps.product'),
                  array('bank b', 'b.id = ip.bank', 'left'),
                  array('pajak pj', 'pj.id = ip.pajak'),
                  array('metode_bayar m', 'm.id = ip.metode_bayar'),
                  array('satuan s', 's.id = ps.satuan', 'left')
              ),
              'where' => "ip.invoice = '" . $invoice . "' and ip.deleted = 0",
              'orderby' => 'ip.id'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['pot_item'] = $this->getListPotonganItem($value['id']);
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPotonganItem($invoice_product) {
  $data = Modules::run('database/get', array(
              'table' => 'invoice_pot_product ipp',
              'field' => array('ipp.*', 'pt.potongan as jenis_potongan'),
              'join' => array(
                  array('potongan pt', 'pt.id = ipp.potongan', 'left')
              ),
              'where' => "ipp.deleted = 0 and ipp.invoice_product = '" . $invoice_product . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getPostDataHeader($value) {
  $data['no_faktur'] = Modules::run('no_generator/generateNoFaktur');
  $data['pembeli'] = $value->pembeli;
  $data['jenis_pembayaran'] = $value->jenis_bayar;
  $data['potongan'] = $value->potongan;
  if ($value->pot_faktur != '') {
   $data['pot_faktur'] = $value->pot_faktur;
  }
  $data['tanggal_faktur'] = date('Y-m-d H:i:s', strtotime($value->tanggal_faktur));
  $data['tanggal_bayar'] = date('Y-m-d', strtotime($value->tanggal_bayar));
  $data['total'] = str_replace('.', '', $value->total);
  return $data;
 }

 public function getDetailCurrentSatuan($product_satuan) {
  $data = Modules::run('database/get', array(
              'table' => 'product_satuan ps',
              'field' => array(
                  'ps.*', 'pst.stock',
                  's.nama_satuan as current_satuan',
                  'sp.nama_satuan as parent_satuan',
                  'psp.id as parent_product_satuan',
                  'psp.qty as qty_parent',
                  'pstp.stock as parent_stock',
                  'pst.id as cur_product_stock',
                  'pstp.id as parent_product_stock'
              ),
              'join' => array(
                  array('product_stock pst', 'pst.product_satuan = ps.id'),
                  array('satuan s', 's.id = ps.satuan'),
                  array('satuan sp', 'sp.id = s.parent', 'left'),
                  array('product_satuan psp', 'psp.satuan = sp.id and psp.product = ps.product', 'left'),
                  array('product_stock pstp', 'pstp.product_satuan = psp.id', 'left'),
              ),
              'where' => "ps.deleted is null or ps.deleted = 0 and ps.id = '" . $product_satuan . "'",
  ));

  //  echo "<pre>";
  //  echo $this->db->last_query();
  //  die;
  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }

  return $result;
 }

 public function penguranganStok($data_item) {
  $current_satuan = $this->getDetailCurrentSatuan($data_item->product_satuan);
  $current_order = $data_item->qty;
  $message = "";
  $is_valid = 1;
  //  echo '<pre>';
  //  print_r($current_satuan);die;
  if (!empty($current_satuan)) {
   $current_stok = $current_satuan['stock'];
   $parent_stok = $current_satuan['parent_stock'] == '' ? 0 : $current_satuan['parent_stock'];
   $qty_parent = $current_satuan['qty_parent'] == '' ? 1 : $current_satuan['qty_parent'];
   // $parent_qty = $current_satuan['qty_parent'] == '' ? 0 : $current_satuan['qty_parent'];

   $konversi_stok = $parent_stok * $qty_parent;
   $sisa_current = $current_stok - $current_order;

   if ($sisa_current < 0) {
    $sisa_current *= -1;

    $total_parent_stok = $konversi_stok - $sisa_current;

    //untuk update current stok
    $current_stok_change = intval($total_parent_stok) % intval($qty_parent);

    //untuk update parent stok
    $parrent_stok_change = ($total_parent_stok - $current_stok_change) / $qty_parent;

    // $parrent_stok_change = $parent_stok - $parrent_stok_change;
    //update product stok current

    if ($parrent_stok_change >= 0) {
     Modules::run('database/_update', 'product_stock', array('stock' => $current_stok_change), array(
         'id' => $current_satuan['cur_product_stock']
     ));

     //update product stok parrent			
     if ($current_satuan['parent_product_stock'] != '') {
      Modules::run('database/_update', 'product_stock', array('stock' => $parrent_stok_change), array(
          'id' => $current_satuan['parent_product_stock']
      ));
     }
    } else {
     $is_valid = 0;
     $message = "Stok " . $current_satuan['parent_satuan'] . ' Kosong, Isi Stok Terlebih Dahulu';
    }
   } else {
    //update product stok current
    $current_stok_change = $current_stok - $current_order;
    if ($current_stok_change >= 0) {
     Modules::run('database/_update', 'product_stock', array('stock' => $current_stok_change), array(
         'id' => $current_satuan['cur_product_stock']
     ));
    } else {
     $is_valid = 0;
     $message = "Stok " . $current_satuan['current_satuan'] . ' Kosong, Isi Stok Terlebih Dahulu';
    }
   }
  }

  return array(
      'is_valid' => $is_valid,
      'message' => $message
  );
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  //		  echo '<pre>';
  //		  print_r($data);die;
  $id = $this->input->post('id');
  $is_valid = false;
  $is_save = true;
  $message = "";

  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);
   //   echo '<pre>';
   //   print_r($post_data);die;
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);

    //invoice product item    
    if (!empty($data->product_item)) {
     foreach ($data->product_item as $value) {
      $post_item['invoice'] = $id;
      $post_item['product_satuan'] = $value->product_satuan;
      $post_item['metode_bayar'] = 1;
      $post_item['pajak'] = $value->pajak;
      $post_item['qty'] = $value->qty;
      $post_item['sub_total'] = str_replace('.', '', $value->sub_total);
      if (trim($value->bank) != '') {
       $post_item['bank'] = $value->bank;
      } else {
       unset($post_item['bank']);
      }

      //pengurangan stok
      $response_stok = $this->penguranganStok($value);
      $is_save = $response_stok['is_valid'];
      $message = $response_stok['message'];

      $invoice_product = Modules::run('database/_insert', 'invoice_product', $post_item);

      //insert potongan
      if (!empty($value->potongan_item)) {
       foreach ($value->potongan_item as $v_pot) {
        $post_pot_item['invoice_product'] = $invoice_product;
        $post_pot_item['potongan'] = $v_pot->potongan;
        $post_pot_item['nilai'] = $v_pot->nilai;

        Modules::run('database/_insert', 'invoice_pot_product', $post_pot_item);
       }
      }

      //insert into product_log_stock
      $post_log_stok['product_satuan'] = $value->product_satuan;
      $post_log_stok['status'] = 'OUT';
      $post_log_stok['stok_kategori'] = '1';
      $post_log_stok['qty'] = $value->qty;
      $post_log_stok['keterangan'] = 'Faktur Pelanggan';
      Modules::run('database/_insert', 'product_log_stock', $post_log_stok);
     }
    }

    //    echo '<pre>';
    //    print_r($data_bank);die;
    //invoice status
    $post_status['invoice'] = $id;
    $post_status['user'] = $this->session->userdata('user_id');
    $post_status['status'] = 'DRAFT';
    Modules::run('database/_insert', 'invoice_status', $post_status);

    //invoice sisa
    $post_sisa['invoice'] = $id;
    $post_sisa['jumlah'] = str_replace('.', '', $data->total);
    Modules::run('database/_insert', 'invoice_sisa', $post_sisa);


    //create jurnal akuntan
    $jurnal = Modules::run('generate_jurnal/insertJurnal', $post_data['no_faktur']);
    $jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Faktur');
    //    echo '<pre>';
    //    print_r($jurnal_struktur);die;
    if (!empty($jurnal_struktur)) {
     foreach ($jurnal_struktur as $value) {
      $post_detail['jurnal'] = $jurnal;
      $post_detail['jurnal_struktur'] = $value['id'];
      $post_detail['jumlah'] = str_replace('.', '', $data->total);
      Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
     }
    }
   } else {
    //update
    unset($post_data['no_faktur']);
    Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));


    if (!empty($data->product_item)) {
     foreach ($data->product_item as $value) {
      $post_item['invoice'] = $id;
      $post_item['product_satuan'] = $value->product_satuan;
      $post_item['metode_bayar'] = $value->metode;
      $post_item['pajak'] = $value->pajak;
      $post_item['qty'] = $value->qty;
      $post_item['sub_total'] = str_replace(',', '', $value->sub_total);
      if (trim($value->bank) != '') {
       $post_item['bank'] = $value->bank;
      } else {
       unset($post_item['bank']);
      }

      if ($value->id != '') {
       if ($value->deleted == 1) {
        $post_item['deleted'] = 1;
       } else {
        $post_item['deleted'] = 0;
       }

       Modules::run('database/_update', 'invoice_product', $post_item, array('id' => $value->id));
      } else {
       Modules::run('database/_insert', 'invoice_product', $post_item);
      }
     }
    }
   }
   if ($is_save) {
    $this->db->trans_commit();
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Faktur";
  $data['title_content'] = 'Data Faktur';
  $content = $this->getDataFaktur($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['akses'] = $this->akses;
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function addItem() {
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['index'] = $_POST['index'];
  echo $this->load->view('product_item', $data, true);
 }

 public function getListBank() {
  $data = Modules::run('database/get', array(
              'table' => 'bank',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getMetodeBayar() {
  $data['list_bank'] = $this->getListBank();
  $data['index'] = $_POST['index'];
  echo $this->load->view('bank_akun', $data, true);
 }

 public function getDataInvoicePrint($invoice) {
  $data = Modules::run('database/get', array(
              'table' => 'invoice_print',
              'where' => "invoice = '" . $invoice . "'",
              'orderby' => 'id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function printFaktur($id) {
  $data_print = $this->getDataInvoicePrint($id);
//		  echo '<pre>';
//		  print_r($data_print);die;

  if (empty($data_print)) {
   $this->printed($id);
  } else {
   if ($data_print[0]['approve'] == 'APPROVED') {
    $this->printed($id);
   } else {
    $url = base_url() . $this->getModuleName() . '/detail/' . $id . "/approve";
    //   echo $url;die;
    $post_print['user'] = $this->session->userdata('user_id');
    $post_print['invoice'] = $id;
    $post_print['approve'] = "ASK";
    Modules::run('database/_insert', 'invoice_print', $post_print);
    redirect($url);
   }
  }
 }

 public function printedOld($id) {
  // $data_pembayaran = array('data');
  // $data = end($data_pembayaran['data']);

  $data['invoice'] = $this->getDetailDataFaktur($id);
  //		 echo '<pre>';
  //		 print_r($data);die;
  $data['invoice_item'] = $this->getListInvoiceItem($id);
  $data['self'] = Modules::run('general/getDetailDataGeneral', 1);
  $mpdf = Modules::run('mpdf/getInitPdfPaperA5');

  $post_print['user'] = $this->session->userdata('user_id');
  $post_print['invoice'] = $id;
  Modules::run('database/_insert', 'invoice_print', $post_print);

  //  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  // echo $view;
  $mpdf->WriteHTML($view);
  $mpdf->Output('Nota Customer - ' . date('Y-m-d') . '.pdf', 'I');
 }

 public function printed($id) {
  // $data_pembayaran = array('data');
  // $data = end($data_pembayaran['data']);

  $data['invoice'] = $this->getDetailDataFaktur($id);
//		  echo '<pre>';
//		  print_r($data);die;
  $data['invoice_item'] = $this->getListInvoiceItem($id);
  $data['self'] = Modules::run('general/getDetailDataGeneral', 1);
  //  echo '<pre>';
  //  print_r($data);die;
  // $data['view_file'] = 'cetak_html';
  // $data['header_data'] = $this->getHeaderJSandCSS();
  // $data['module'] = $this->getModuleName();
  // $data['title'] = "Cetak Pengiriman";
  // $data['title_content'] = 'Cetak Pengiriman';
  // echo Modules::run('template', $data);
  echo $this->load->view('cetak_html', $data, true);
 }

 public function approvePrinted() {
  $invoice = $_POST['invoice'];

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_print['user'] = $this->session->userdata('user_id');
   $post_print['invoice'] = $invoice;
   $post_print['approve'] = "APPROVED";
   Modules::run('database/_insert', 'invoice_print', $post_print);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getListPotongan() {
  $data = Modules::run('database/get', array(
              'table' => 'potongan',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getPotongan() {
  $data['list_potongan'] = $this->getListPotongan();
  $data['index'] = $_POST['index'];
  //  echo '<pre>';
  //  print_r($data);die;
  echo $this->load->view('potongan_view', $data, true);
 }

 public function getCurrentStok($product_satuan) {
  $data = Modules::run('database/get', array(
              'table' => 'product_stock ps',
              'field' => array('ps.*'),
              'where' => "ps.deleted = 0 and ps.product_satuan = '" . $product_satuan . "'"
  ));

  $total = 0;
  $result = array();
  if (!empty($data)) {
   $data = $data->row_array();
   $total = $data['stock'];
  }


  return $total;
 }

 public function execUbahQty() {
  //  echo '<pre>';
  //  print_r($_POST);die;
  $current_stok = $this->getCurrentStok($_POST['product_satuan']);

  $is_valid = false;
  $this->db->trans_begin();
  try {
   //update invoice_product
   $post_ip['qty'] = $_POST['qty_ubah'];
   $post_ip['sub_total'] = $_POST['sub_total'];
   Modules::run('database/_update', 'invoice_product', $post_ip, array('id' => $_POST['invoice_product']));

   //die;
   //insert into product_log_stock
   $post_log_stok['product_satuan'] = $_POST['product_satuan'];
   $post_log_stok['status'] = 'IN';
   $post_log_stok['stok_kategori'] = '1';
   $post_log_stok['qty'] = $_POST['total_qty_back'];
   $post_log_stok['keterangan'] = 'Faktur Pelanggan';
   Modules::run('database/_insert', 'product_log_stock', $post_log_stok);


   //update stock    
   $post_stock['stock'] = $current_stok + $_POST['total_qty_back'];
   Modules::run(
           'database/_update',
           'product_stock',
           $post_stock,
           array('product_satuan' => $_POST['product_satuan'])
   );

   $data_invoice_product = $this->getListInvoiceItem($_POST['invoice']);

   $total = 0;
   foreach ($data_invoice_product as $value) {
    $total += $value['sub_total'];
   }

   //update invoice
   $post_invoice['total'] = $total;
   Modules::run('database/_update', $this->getTableName(), $post_invoice, array('id' => $_POST['invoice']));

   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function updateTanggalBayar() {
  //  echo '<pre>';
  //  print_r($_POST);die;

  $is_valid = false;
  $this->db->trans_begin();
  try {
   //update
   $post_invoice['tanggal_bayar'] = $_POST['tgl_bayar'];
   Modules::run('database/_update', $this->getTableName(), $post_invoice, array('id' => $_POST['id']));

   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function updateTanggalFaktur() {
  //  echo '<pre>';
  //  print_r($_POST);die;

  $is_valid = false;
  $this->db->trans_begin();
  try {
   //update
   $post_invoice['tanggal_faktur'] = $_POST['tgl_faktur'];
   Modules::run('database/_update', $this->getTableName(), $post_invoice, array('id' => $_POST['id']));

   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function updateMetodeBayar() {
  //  echo '<pre>';
  //  print_r($_POST);die;

  $is_valid = false;
  $this->db->trans_begin();
  try {
   //update
   $post_invoice['jenis_pembayaran'] = $_POST['metode_bayar'];
   Modules::run('database/_update', $this->getTableName(), $post_invoice, array('id' => $_POST['id']));

   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function simpanItem() {
  $data_item = json_decode($_POST['data']);
  $id = $this->input->post('id');
  $no_faktur = $this->input->post('no_faktur');

  $data_jurnal = $this->getDetailJurnal($no_faktur);

  $is_valid = false;
  $is_save = true;
  $message = "";

  $this->db->trans_begin();
  try {
   //invoice product item    
   if (!empty($data_item)) {
    foreach ($data_item as $value) {
     $post_item['invoice'] = $id;
     $post_item['product_satuan'] = $value->product_satuan;
     $post_item['metode_bayar'] = 1;
     $post_item['pajak'] = $value->pajak;
     $post_item['qty'] = $value->qty;
     $post_item['sub_total'] = str_replace('.', '', $value->sub_total);
     if (trim($value->bank) != '') {
      $post_item['bank'] = $value->bank;
     } else {
      unset($post_item['bank']);
     }

     //pengurangan stok
     $response_stok = $this->penguranganStok($value);
     $is_save = $response_stok['is_valid'];
     $message = $response_stok['message'];

     $invoice_product = Modules::run('database/_insert', 'invoice_product', $post_item);

     //insert potongan
     if (!empty($value->potongan_item)) {
      foreach ($value->potongan_item as $v_pot) {
       $post_pot_item['invoice_product'] = $invoice_product;
       $post_pot_item['potongan'] = $v_pot->potongan;
       $post_pot_item['nilai'] = $v_pot->nilai;

       Modules::run('database/_insert', 'invoice_pot_product', $post_pot_item);
      }
     }

     //insert into product_log_stock
     $post_log_stok['product_satuan'] = $value->product_satuan;
     $post_log_stok['status'] = 'OUT';
     $post_log_stok['stok_kategori'] = '1';
     $post_log_stok['qty'] = $value->qty;
     $post_log_stok['keterangan'] = 'Faktur Pelanggan';
     Modules::run('database/_insert', 'product_log_stock', $post_log_stok);
    }
   }

   $data_list_item = $this->getListInvoiceItem($id);

   $total = 0;
   if (!empty($data_list_item)) {
    foreach ($data_list_item as $value) {
     $total += $value['sub_total'];
    }
   }

   //invoice
   $post_invoice['total'] = str_replace('.', '', $total);
   Modules::run('database/_update', $this->getTableName(), $post_invoice, array('id' => $id));

   //invoice sisa
   $post_sisa['jumlah'] = str_replace('.', '', $total);
   Modules::run('database/_update', 'invoice_sisa', $post_sisa, array('invoice' => $id));

   //jurnal_detail
   $post_jurnal['jumlah'] = str_replace('.', '', $total);
   Modules::run('database/_update', 'jurnal_detail', $post_jurnal, array('jurnal' => $data_jurnal['id']));

   if ($is_save) {
    $this->db->trans_commit();
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function getDetailJurnal($no_faktur) {
  $data = Modules::run('database/get', array(
              'table' => 'jurnal jn',
              'field' => array(
                  'jn.*'
              ),
              'where' => "jn.ref = '" . $no_faktur . "' and jn.deleted = 0",
              'orderby' => 'jn.id'
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }

  return $result;
 }

 public function getDetailJurnalItem($jurnal) {
  $data = Modules::run('database/get', array(
              'table' => 'jurnal_detail jn',
              'field' => array(
                  'jn.*'
              ),
              'where' => "jn.jurnal = '" . $jurnal . "' and jn.deleted = 0",
              'orderby' => 'jn.id'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

}
