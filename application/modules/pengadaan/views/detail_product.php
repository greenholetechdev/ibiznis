<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-striped table-bordered table-list-draft" id="tb_product">
    <thead>
     <tr class="bg-primary-light text-white">
      <th>Produk</th>
      <th>Satuan</th>
      <th>Harga</th>
      <th>Jumlah</th>
      <th>Sub Total</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($list_proc_item)) { ?>
      <?php $index = 0; ?>
      <?php $temp = ""; ?>
      <?php foreach ($list_proc_item as $value) { ?>
       <tr data_id="<?php echo $value['id'] ?>"> 
        <td>
         <?php echo $value['nama_product'] == $temp  ? '' : $value['nama_product'] ?>
        </td>
        <td>
         <?php echo $value['nama_satuan'] ?>
        </td>
        <td>
         <?php echo 'Rp, ' . number_format($value['harga']) ?>
        </td>
        <td>
         <?php echo $value['qty'] ?>
        </td>
        <td><?php echo 'Rp, ' . number_format($value['sub_total']) ?></td>
       </tr>

       <?php $index += 1; ?>
       <?php $temp = $value['nama_product']; ?>
      <?php } ?>
     <?php } ?> 
    </tbody>
   </table>
  </div>
 </div>
</div>