<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Surat Jalan
     </div>
     <div class='col-md-3'>
      <?php echo $no_surat_jalan ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $no_faktur ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_faktur ?>
     </div>     
    </div>
    <br/>
    <br/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <div class="form-item">
       <?php echo $this->load->view('form_product'); ?>
      </div>      
     </div>
    </div>
    <br/>


    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total"><?php echo isset($total) ? number_format($total) : '0' ?></label></h4>
     </div>
    </div>
    <hr/>    
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger" onclick="SuratJalan.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;
      <button id="" class="btn btn-danger-baru" onclick="SuratJalan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
