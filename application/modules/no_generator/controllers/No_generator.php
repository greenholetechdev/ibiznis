<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');

class No_generator extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function generateInvoice($frontID,$table) {
  $no_invoice = $frontID . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => $table,
  'like' => array(
  array('no_invoice', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_invoice']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoFaktur() {
  $no_invoice = 'INV' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'invoice',
  'like' => array(
  array('no_faktur', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoFakturBayar() {
  $no_invoice = 'PAY' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'payment',
  'like' => array(
  array('no_faktur_bayar', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur_bayar']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoFakturBayarKasbon() {
  $no_invoice = 'PAYBON' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'kasbon_payment',
  'like' => array(
  array('no_faktur', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoFakturBayarSebagian() {
  $no_invoice = 'PAYSB' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'partial_payment',
  'like' => array(
  array('no_faktur_bayar', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur_bayar']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoGajiBayar() {
  $no_invoice = 'PAYSLIP' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'payroll',
  'like' => array(
  array('no_faktur', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoJurnal() {
  $no_ = 'JURNAL' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'jurnal',
  'like' => array(
  array('no_jurnal', $no_)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_, '', $data['no_jurnal']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_ .= $seq;
  return $no_;
 }
 
 
 public function generateNoFakturKasbon() {
  $no_invoice = 'BON' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'kasbon',
  'like' => array(
  array('no_kasbon', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_kasbon']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoFakturPengadaan() {
  $no_invoice = 'PROC' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'procurement',
  'like' => array(
  array('no_faktur', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoFakturReturPengadaan() {
  $no_invoice = 'REPROC' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'procurement_retur',
  'like' => array(
  array('no_faktur', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoRute() {
  $no_invoice = 'RUTE' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'sales_rute',
  'like' => array(
  array('no_rute', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_rute']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoFakturReturPelanggan() {
  $no_invoice = 'RECUST' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'return_penjualan',
  'like' => array(
  array('no_retur', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_retur']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoFakturReimburse() {
  $no_invoice = 'REIM' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'reimburse',
  'like' => array(
  array('no_faktur', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoSuratJalan() {
  $no_invoice = 'SJ' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'surat_jalan',
  'like' => array(
  array('no_surat_jalan', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_surat_jalan']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoOrder() {
  $no_invoice = 'OR' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'order',
  'like' => array(
  array('no_order', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_order']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoReturBarang() {
  $no_invoice = 'RETURBR' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'retur_barang',
  'like' => array(
  array('no_retur', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_retur']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }
 
 public function generateNoPengirimanJalan() {
  $no_invoice = 'SEND' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'pengiriman',
  'like' => array(
  array('no_pengiriman', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_pengiriman']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  return $no_invoice;
 }

}
