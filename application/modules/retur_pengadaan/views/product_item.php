<td>
 <select class="form-control required" id="product<?php echo $index ?>" 
         error="Produk" onchange="Pengadaan.hitungSubTotal(this, 'product')">
  <option value="" harga="0">Pilih Produk</option>
  <?php if (!empty($list_product)) { ?>
   <?php foreach ($list_product as $value) { ?>
    <?php $selected = '' ?>
    <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['product'] ?></option>
   <?php } ?>
  <?php } ?>
 </select>
</td>
<td>
 <div class="select_content">
  <div class="top">
   <select class="form-control required" id="satuan<?php echo $index ?>" error="Satuan"
           onchange="Pengadaan.hitungSubTotal(this, 'satuan')">
    <option value="">Pilih Satuan</option>
    <?php if (!empty($list_satuan)) { ?>
     <?php foreach ($list_satuan as $value) { ?>
      <?php $selected = '' ?>
      <option <?php echo $selected ?> value="<?php echo $value['satuan'] ?>"><?php echo $value['nama_satuan'] ?></option>
     <?php } ?>
    <?php } ?>
   </select>
  </div>
  <div class="text-right" id="action_remove">
   
  </div>
 </div>
</td>
<td>
 <input type="number" value="0" min="0" id="harga" 
        class="form-control text-right" 
        onkeyup="Pengadaan.hitungSubTotal(this, 'harga')" 
        onchange="Pengadaan.hitungSubTotal(this, 'harga')"/>
</td>
<td class="text-center">
 <input type="number" value="1" min="1" id="jumlah" 
        class="form-control text-right" 
        onkeyup="Pengadaan.hitungSubTotal(this, 'jumlah')" 
        onchange="Pengadaan.hitungSubTotal(this, 'jumlah')"/>
</td>      
<td class="text-center">
 <label id="sub_total">0</label>
</td>      
<td class="text-center">
 <i class="mdi mdi-delete mdi-18px" onclick="Pengadaan.deleteItem(this)"></i>
 &nbsp;
 <i class="fa fa-plus" onclick="Pengadaan.addSatuanContent(this)"></i>
</td>


<script>
 $(function () {
  $("#product<?php echo $index ?>").select2();
  $("#metode<?php echo $index ?>").select2();
  $("#satuan<?php echo $index ?>").select2();
 });
</script>