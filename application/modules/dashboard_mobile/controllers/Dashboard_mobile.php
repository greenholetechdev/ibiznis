<?php

class Dashboard_mobile extends MX_Controller {

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/raphael/raphael.min.js"></script>',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/faktur_pelanggan.js"></script>'
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['penjualan'] = $this->getTopPenjualan();
  $data['total_pj'] = $this->getPenjualan();
  $data['total_pemasukan'] = $this->getTotalPemasukan();
  $data['tagihan'] = Modules::run('laba_rugi/getDataTagihan');
  $data['vendor'] = Modules::run('laba_rugi/getDataBayarVendor');
  $data['lain'] = Modules::run('laba_rugi/getDataBayarLain');
  $data['total_product'] = count($this->getTotalProduct());
  $data['total_customer'] = count($this->getTotalCustomer());
  $data['data_penjualan'] = $this->getDataGrafikPenjualan();
  $data['data_kas_kredit'] = $this->getDataGrafikKasKredit();
  $data['data_kas_debit'] = $this->getDataGrafikKasDebit();
//  echo '<pre>';
//  print_r($data['data_kas_debit']);
//  die;
  $data['date_now'] = Modules::run('helper/getIndoDate', date('Y-m-d'), false);
  echo $this->load->view('v_index', $data, true);
 }

 public function getTotalProduct() {
  $data = Modules::run('database/get', array(
              'table' => 'product',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getTotalCustomer() {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getPenjualan() {
  $date = date('Y');
  $query = <<<QUERY
  
select sum(total) as total from invoice where tanggal_faktur like '%$date%'
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $total = 0;
  if (!empty($data)) {
   $total = $data->row_array()['total'];
  }

  $query = <<<QUERY
  
select count(*) as jumlah from invoice where tanggal_faktur like '%$date%'
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $unit = 0;
  if (!empty($data)) {
   $unit = $data->row_array()['jumlah'];
  }

  return array(
      'unit' => $unit,
      'total' => $total
  );
 }

 public function getDataGrafikPenjualan() {

  $date = date('Y') . '-';

  $result = array();
  $total = 0;
  $total_max = 0;
  for ($i = 1; $i < 12; $i ++) {
   $i = $i < 10 ? '0' . $i : $i;
   $date_str = $date . $i;
   $query = "SELECT * FROM invoice WHERE createddate like '%" . $date_str . "%' and deleted = 0";

   $data = Modules::run("database/get_custom", $query);


   $result[] = empty($data) ? 0 : count($data->result_array());
   $total += empty($data) ? 0 : count($data->result_array());
   
   if(!empty($data)){
    if($total_max > count($data->result_array())){
     $total_max = count($data->result_array());
    }
   }
  }
  $str = implode(',', $result);
  return array(
      'data' => $str,
      'total' => $total_max
  );
 }
 
 public function getDataGrafikKasKredit() {

  $date = date('Y') . '-';

  $result = array();
  $total = 0;
  $total_max = 0;
  for ($i = 1; $i < 12; $i ++) {
   $i = $i < 10 ? '0' . $i : $i;
   $date_str = $date . $i;
   $query = "select js.id, c.keterangan  
	, ct.jenis
from jurnal_detail jd
join jurnal_struktur js
	on jd.jurnal_struktur = js.id
join feature f
	on js.feature = f.id
join coa c
	on c.id = js.coa
join coa_type ct
	on ct.id = js.coa_type
	where c.keterangan = 'Bank & Kas' and jd.deleted = 0 and 
 jd.createddate like '%".$date_str."%' and ct.jenis = 'KREDIT'";

   $data = Modules::run("database/get_custom", $query);
//   echo "<pre>";
//   echo $this->db->last_query();
//   die;

   $result[] = empty($data) ? 0 : count($data->result_array());
   $total += empty($data) ? 0 : count($data->result_array());
   
   if(!empty($data)){
    if($total_max > count($data->result_array())){
     $total_max = count($data->result_array());
    }
   }
  }
  $str = implode(',', $result);
  return array(
      'data' => $str,
      'total' => $total_max
  );
 }
 
 public function getDataGrafikKasDebit() {

  $date = date('Y') . '-';

  $result = array();
  $total = 0;
  $total_max = 0;
  for ($i = 1; $i < 12; $i ++) {
   $i = $i < 10 ? '0' . $i : $i;
   $date_str = $date . $i;
   $query = "select js.id, c.keterangan  
	, ct.jenis
from jurnal_detail jd
join jurnal_struktur js
	on jd.jurnal_struktur = js.id
join feature f
	on js.feature = f.id
join coa c
	on c.id = js.coa
join coa_type ct
	on ct.id = js.coa_type
	where c.keterangan = 'Bank & Kas' and jd.deleted = 0 and 
 jd.createddate like '%".$date_str."%' and ct.jenis = 'DEBIT'";

   $data = Modules::run("database/get_custom", $query);
//   echo "<pre>";
//   echo $this->db->last_query();
//   die;

   $result[] = empty($data) ? 0 : count($data->result_array());
   $total += empty($data) ? 0 : count($data->result_array());
   
   if(!empty($data)){
    if($total_max > count($data->result_array())){
     $total_max = count($data->result_array());
    }
   }
  }
  $str = implode(',', $result);
  return array(
      'data' => $str,
      'total' => $total_max
  );
 }

 public function getTotalPemasukan() {
  $date = date('Y');
  $query = "select
	sum(i.total) as total
	from invoice i
	join (select max(id) id, invoice from invoice_status group by invoice) iss
		on iss.invoice = i.id
	join invoice_status isa on isa.id = iss.id
	where isa.status = 'PAID' and i.tanggal_faktur like '%$date%'
";

//  echo '<pre>';
//  echo $query;die;
  $data = Modules::run('database/get_custom', $query);

  $total = 0;
  if (!empty($data)) {
   $total = $data->row_array()['total'];
  }
//  echo $total;die;
  return $total;
 }

 public function getTopPenjualan($keyword = '') {
  $data = Modules::run('database/get', array(
              'table' => 'invoice i',
              'field' => array('i.*', 'p.nama as nama_pembeli',
                  'p.no_hp', 'p.alamat', 'isa.status'),
              'join' => array(
                  array('pembeli p', 'i.pembeli = p.id'),
                  array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'i.id = iss.invoice'),
                  array('invoice_status isa', 'isa.id= iss.id'),
              ),
              'limit' => 5,
              'orderby' => 'i.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDataPenjualanStr() {
  $date = date('Y-m') . '-';

  $result = array();
  $total = 0;
  for ($i = 1; $i < 32; $i ++) {
   $i = $i < 10 ? '0' . $i : $i;
   $date_str = $date . $i;
   $query = "SELECT * FROM pembelian WHERE createddate = '" . $date_str . "'";

   $data = Modules::run("database/get_custom", $query);


   $result[] = empty($data) ? 0 : count($data->result_array());
   $total += empty($data) ? 0 : count($data->result_array());
  }
  $str = implode(',', $result);
  return array(
      'data' => $str,
      'total' => $total
  );
 }

 public function getDataKredit() {
  $date = date('Y') . '-';

  $result = array();
  $total = 0;
  for ($i = 1; $i < 13; $i ++) {
   $i = $i < 10 ? '0' . $i : $i;
   $date_str = $date . $i;
   $query = "SELECT * FROM pembayaran_product WHERE createddate LIKE '%" . $date_str . "%'";

   $data = Modules::run("database/get_custom", $query);

   $result[] = empty($data) ? 0 : count($data->result_array());
   $total += empty($data) ? 0 : count($data->result_array());
  }
  $str = implode(',', $result);
  return array(
      'data' => $str,
      'total' => $total
  );
 }

}
