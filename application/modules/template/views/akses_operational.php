<?php if ($this->session->userdata('hak_akses') == "operational") { ?>
 <ul class="sidebar-menu" data-widget="tree">
  <li class="header">MAIN NAVIGATION</li>
  <li class="active"><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>
  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Penjualan</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'pelanggan' ?>"><i class="fa fa-file-text-o"></i> Pelanggan </a></li>
    <li><a href="<?php echo base_url() . 'produk' ?>"><i class="fa fa-file-text-o"></i> Produk </a></li>
    <li><a href="<?php echo base_url() . 'kategori_produk' ?>"><i class="fa fa-file-text-o"></i> Kategori Produk </a></li>
    <li><a href="<?php echo base_url() . 'tipe_produk' ?>"><i class="fa fa-file-text-o"></i> Tipe Produk </a></li>
    <li><a href="<?php echo base_url() . 'order' ?>"><i class="fa fa-file-text-o"></i> Order </a></li>
    <li><a href="<?php echo base_url() . 'returorder' ?>"><i class="fa fa-file-text-o"></i> Retur Order </a></li>
    <li><a href="<?php echo base_url() . 'surat_jalan' ?>"><i class="fa fa-file-text-o"></i> Surat Jalan </a></li>
    <!--<li><a href="<?php echo base_url() . 'retur' ?>"><i class="fa fa-file-text-o"></i> Retur </a></li>-->
    <li><a href="<?php echo base_url() . 'rute_salesman' ?>"><i class="fa fa-file-text-o"></i> Rute Salesman </a></li>
    <li><a href="<?php echo base_url() . 'lacak_salesman' ?>"><i class="fa fa-file-text-o"></i> Lacak Salesman </a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Penagihan</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <!--<li><a href="<?php echo base_url() . 'faktur' ?>"><i class="fa fa-file-text-o"></i> Faktur bersyarat </a></li>-->
    <li><a href="<?php echo base_url() . 'faktur_pelanggan' ?>"><i class="fa fa-file-text-o"></i> Faktur </a></li>
    <li><a href="<?php echo base_url() . 'retur_barang' ?>"><i class="fa fa-file-text-o"></i> Retur </a></li>
    <!--<li><a href="<?php echo base_url() . 'persyaratan' ?>"><i class="fa fa-file-text-o"></i> Syarat Pembayaran </a></li>-->
    <!--<li><a href="<?php echo base_url() . 'metode_bayar' ?>"><i class="fa fa-file-text-o"></i> Metode Pembayaran </a></li>-->
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Pembelian</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'supplier' ?>"><i class="fa fa-file-text-o"></i> Vendor </a></li>
    <li><a href="<?php echo base_url() . 'pengadaan' ?>"><i class="fa fa-file-text-o"></i> Pengadaan </a></li>
    <li><a href="<?php echo base_url() . 'retur_pengadaan' ?>"><i class="fa fa-file-text-o"></i> Retur </a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Pembayaran</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'tagihan' ?>"><i class="fa fa-file-text-o"></i> Tagihan </a></li>
    <li><a href="<?php echo base_url() . 'pembayaran' ?>"><i class="fa fa-file-text-o"></i> Biaya Umum </a></li>
    <!--<li><a href="<?php echo base_url() . 'payment' ?>"><i class="fa fa-file-text-o"></i> Faktur </a></li>-->
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Pengiriman</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'pengiriman' ?>"><i class="fa fa-file-text-o"></i> Paket </a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Inventori</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'gudang' ?>"><i class="fa fa-file-text-o"></i> Gudang </a></li>
    <li><a href="<?php echo base_url() . 'rak' ?>"><i class="fa fa-file-text-o"></i> Rak </a></li>
    <li><a href="<?php echo base_url() . 'unit' ?>"><i class="fa fa-file-text-o"></i> Satuan </a></li>
    <li><a href="<?php echo base_url() . 'satuan' ?>"><i class="fa fa-file-text-o"></i> Produk Satuan </a></li>
    <li><a href="<?php echo base_url() . 'product_stock' ?>"><i class="fa fa-file-text-o"></i> Stok </a></li>
   </ul>
  </li>	

  <li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
 </ul>

<?php } ?>
