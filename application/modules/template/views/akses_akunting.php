<?php if ($this->session->userdata('hak_akses') == "manajemen accounting") { ?>
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="active"><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Pembayaran</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'tagihan' ?>"><i class="fa fa-file-text-o"></i> Tagihan </a></li>
				<li><a href="<?php echo base_url() . 'pembayaran' ?>"><i class="fa fa-file-text-o"></i> Biaya Umum </a></li>
				<!--<li><a href="<?php echo base_url() . 'payment' ?>"><i class="fa fa-file-text-o"></i> Faktur </a></li>-->
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Akuntansi</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'coa' ?>"><i class="fa fa-file-text-o"></i> Bagan Akun </a></li>
				<li><a href="<?php echo base_url() . 'jurnal_struktur' ?>"><i class="fa fa-file-text-o"></i> Jurnal Struktur </a></li>
				<li><a href="<?php echo base_url() . 'bank_akun' ?>"><i class="fa fa-file-text-o"></i> Bank Akun </a></li>
				<li><a href="<?php echo base_url() . 'kas' ?>"><i class="fa fa-file-text-o"></i> Kas & Bank </a></li>
				<!--<li><a href="<?php echo base_url() . 'mata_uang' ?>"><i class="fa fa-file-text-o"></i> Mata Uang </a></li>-->
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Penggajian</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'payroll' ?>"><i class="fa fa-file-text-o"></i> Gaji </a></li>
				<li><a href="<?php echo base_url() . 'payroll_category' ?>"><i class="fa fa-file-text-o"></i> Kategori </a></li>
				<li><a href="<?php echo base_url() . 'periode' ?>"><i class="fa fa-file-text-o"></i> Periode </a></li>
				<li><a href="<?php echo base_url() . 'reimburse' ?>"><i class="fa fa-file-text-o"></i> Reimburse </a></li>
				<li><a href="<?php echo base_url() . 'kasbon' ?>"><i class="fa fa-file-text-o"></i> Kasbon </a></li>
				<li><a href="<?php echo base_url() . 'kasbon_payment' ?>"><i class="fa fa-file-text-o"></i> Kasbon Bayar</a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Zakat</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'zakat_usaha' ?>"><i class="fa fa-file-text-o"></i> Zakat Usaha </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Laporan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'laba_rugi' ?>"><i class="fa fa-file-text-o"></i> Laba Rugi </a></li>
				<li><a href="<?php echo base_url() . 'bagi_hasil' ?>"><i class="fa fa-file-text-o"></i> Bagi Hasil </a></li>
				<li><a href="<?php echo base_url() . 'arus_kas' ?>"><i class="fa fa-file-text-o"></i> Arus Kas </a></li>
				<li><a href="<?php echo base_url() . 'jurnal' ?>"><i class="fa fa-file-text-o"></i> Jurnal </a></li>
				<li><a href="<?php echo base_url() . 'lappenjualan' ?>"><i class="fa fa-file-text-o"></i> Penjualan </a></li>
				<li><a href="<?php echo base_url() . 'ringkasan_pemasukan' ?>"><i class="fa fa-file-text-o"></i> Ringkasan Pemasukan </a></li>
				<li><a href="<?php echo base_url() . 'ringkasan_pengeluaran' ?>"><i class="fa fa-file-text-o"></i> Ringkasan Pengeluaran </a></li>
				<li><a href="<?php echo base_url() . 'pemasukan_pengeluaran' ?>"><i class="fa fa-file-text-o"></i> Pemasukan vs Pengeluaran </a></li>
			</ul>
		</li>

		<li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
	</ul>

<?php } ?>
