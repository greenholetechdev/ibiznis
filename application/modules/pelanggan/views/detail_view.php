<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <br>
   <div class="box-body"> 
    <div class="row">
     <div class='col-md-2 text-bold'>
      Nama
     </div>
     <div class='col-md-3'>
      <?php echo $nama ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-2 text-bold'>
      No HP
     </div>
     <div class='col-md-3'>
      <?php echo $no_hp ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-2 text-bold'>
      Email
     </div>
     <div class='col-md-3'>
      <?php echo $email ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-2 text-bold'>
      Kelurahan
     </div>
     <div class='col-md-3'>
      <?php echo $kelurahan ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-2 text-bold'>
      Kecamatan
     </div>
     <div class='col-md-3'>
      <?php echo $kecamatan ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-2 text-bold'>
      Kota
     </div>
     <div class='col-md-3'>
      <?php echo $kota ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-2 text-bold'>
      Alamat
     </div>
     <div class='col-md-3'>
      <?php echo $alamat ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="Pelanggan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
