<input type='hidden' name='' id='email' class='form-control' value='<?php echo isset($email) ? $email : '' ?>'/>
<input type='hidden' name='' id='no_hp' class='form-control' value='<?php echo isset($no_hp) ? $no_hp : '' ?>'/>
<input type='hidden' name='' id='no_faktur' class='form-control' value='<?php echo isset($no_faktur) ? $no_faktur : '' ?>'/>

<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Masukan Pesan Notifikasi</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Pesan
     </div>
     <div class='col-md-9'>
      <textarea id="pesan" class="form-control required" error="Pesan"></textarea>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="SendNotif.kirimNotif('<?php echo isset($pembayaran_rumah) ? $pembayaran_rumah : '' ?>')">Proses</button>
     </div>
    </div>
   </div>
  </div>
 </div>
