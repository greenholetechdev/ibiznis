<?php

class Faktur extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'faktur';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/faktur.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pembayaran_product';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Faktur";
  $data['title_content'] = 'Data Faktur';
  $content = $this->getDataFaktur();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataFaktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('p.nama', $keyword),
   array('p.no_hp', $keyword),
   array('p.alamat', $keyword),
   array('phr.no_invoice', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' pr',
  'field' => array('pr.*', 'phr.no_invoice', 'p.nama as nama_pembeli',
  'p.no_hp', 'p.alamat'),
  'join' => array(
  array('pembeli_has_product phr', 'pr.pembeli_has_product = phr.id'),
  array('pembeli p', 'phr.pembeli = p.id'),
  ),
  'like' => $like,
  'is_or_like' => true,
  'orderby' => 'pr.id desc'
  ));

  return $total;
 }

 public function getDataFaktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('p.nama', $keyword),
   array('p.no_hp', $keyword),
   array('p.alamat', $keyword),
   array('phr.no_invoice', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' pr',
  'field' => array('pr.*', 'phr.no_invoice', 'p.nama as nama_pembeli',
  'p.no_hp', 'p.alamat', 'sp.status'),
  'join' => array(
  array('pembeli_has_product phr', 'pr.pembeli_has_product = phr.id'),
  array('pembeli p', 'phr.pembeli = p.id'),
  array('status_pembayaran sp', 'pr.status_pembayaran = sp.id'),
  ),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'orderby' => 'pr.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataFaktur($keyword)
  );
 }

 public function getDetailDataFaktur($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Faktur";
  $data['title_content'] = 'Tambah Faktur';
  echo Modules::run('template', $data);
 }

 public function bayar($id) {
  $data['content'] = $this->getDataPembayaranDetail($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Bayar Faktur";
  $data['title_content'] = 'Bayar Faktur';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data['content'] = $this->getDataPembayaranDetail($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Faktur";
  $data['title_content'] = 'Detail Faktur';
  $data['id_faktur'] = $id;
  echo Modules::run('template', $data);
 }

 public function getDataProdukDibeli($id) {
  $data = Modules::run('database/get', array(
  'table' => 'pembayaran_product pr',
  'field' => array('pr.*', 'sp.status as status_bayar', 
  'p.nama as nama_pembeli', 'r.product'),
  'join' => array(
   array('status_pembayaran sp', 'pr.status_pembayaran = sp.id'),
   array('pembeli_has_product phr', 'pr.pembeli_has_product = phr.id'),
   array('pembeli p', 'phr.pembeli = p.id'),
   array('product r', 'phr.product = r.id'),
  ),
  'where' => array('pr.id'=> $id)
  ));
  
  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }
 
 public function printFaktur($no_invoice, $pr) {
  $data_pembayaran = $this->getDetailDataPembayaranByInvoice($no_invoice);
  $data = end($data_pembayaran['data']);

  $data['is_angsuran'] = $data_pembayaran['is_angsuran'];
  $data['angsuran_ke'] = 0;
  if ($data['is_angsuran']) {
   $data['angsuran_ke'] = count($data_pembayaran['data']);
  }

  $data['produk'] = $this->getDataProdukDibeli($pr);
  $mpdf = Modules::run('mpdf/getInitPdf');

//  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output('Nota Customer - ' . date('Y-m-d') . '.pdf', 'I');
 }

 public function getPostDataHeader($value) {
  $data['pembeli_has_product'] = $value->pembelian_product;
  $data['status_pembayaran'] = $value->status == 'kredit' ? '2' : '1';
  $data['tgl_bayar'] = $value->tgl_bayar;
  return $data;
 }

 public function getPostDataAngsuran($value, $product_bayar) {
  $data['no_invoice'] = Modules::run('no_generator/generateInvoice', 'AGS', 'pembayaran_has_angsuran');
  $data['pembayaran_product'] = $product_bayar;
  $data['total_bayar'] = str_replace(',', '', $value->jumlah_bayar);
  $data['tgl_angsuran'] = $value->tgl_bayar;
  return $data;
 }

 public function getPostDataCash($value, $product_bayar) {
  $data['no_invoice'] = Modules::run('no_generator/generateInvoice', 'CASH', 'pembayaran_has_cash');
  $data['pembayaran_product'] = $product_bayar;
  $data['total_bayar'] = str_replace(',', '', $value->jumlah_bayar);
  $data['tgl_bayar'] = $value->tgl_bayar;
  return $data;
 }

 public function prosesBayar() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;
  $product_bayar = $data->pembayaran_product_id;
  $this->db->trans_begin();
  try {
   $post_bayar_product = $this->getPostDataHeader($data);
   if ($data->pembayaran_product_id == '') {
    $product_bayar = Modules::run('database/_insert', $this->getTableName(), $post_bayar_product);
   }

   if ($data->status == 'kredit') {
    $belum_dicicil = str_replace('x', '', $data->belum_dicicil);
    if (intval($belum_dicicil) == 1) {
     //update lunas
    }
    //insert pembayaran_angsuran
    $post_pembayaran_angsuran = $this->getPostDataAngsuran($data, $product_bayar);
    Modules::run('database/_insert', 'pembayaran_has_angsuran', $post_pembayaran_angsuran);
   } else {
    //insert pembayaran_cash
    $post_pembayaran_cash = $this->getPostDataCash($data, $product_bayar);
    Modules::run('database/_insert', 'pembayaran_has_cash', $post_pembayaran_cash);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'product_bayar' => $product_bayar));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Faktur";
  $data['title_content'] = 'Data Faktur';
  $content = $this->getDataFaktur($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getDataPembelian($no_invoice) {
  $data = Modules::run('database/get', array(
  'table' => 'pembeli_has_product phr',
  'field' => array('phr.*', 'r.product as nama_product',
  'p.nama as nama_pembeli', 'p.no_hp', 'p.alamat',
  'sp.status', 'rhhjc.harga as harga_cash',
  'rhhjk.harga as harga_kredit',
  'pha.id as pembeli_punya_angsuran',
  'rhha.harga as harga_angsuran',
  'a.periode_tahun', 'a.ansuran as angsuran_time', 'pr.id as pembayaran_product_id'),
  'join' => array(
  array('pembeli p', 'phr.pembeli = p.id'),
  array('product r', 'phr.product = r.id'),
  array('status_pembelian sp', 'phr.status_pembelian = sp.id'),
  array('product_has_harga_jual_tunai rhhjk', 'rhhjk.product = r.id and rhhjk.period_end is null'),
  array('product_has_harga_jual_pokok rhhjc', 'rhhjc.product = r.id and rhhjc.period_end is null'),
  array('pembeli_has_angsuran pha', 'phr.id = pha.pembeli_has_product', 'left'),
  array('product_has_harga_angsuran rhha', 'pha.product_has_harga_angsuran = rhha.id and rhha.period_end is null', 'left'),
  array('ansuran a', 'rhha.ansuran = a.id', 'left'),
  array('pembayaran_product pr', 'pr.pembeli_has_product = phr.id', 'left'),
  ),
  'where' => array('phr.no_invoice' => $no_invoice)
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['sudah_angsur'] = '';
    if (trim(strtolower($value['status'])) == 'kredit') {
     $value['sudah_angsur'] = $this->getTotalPembeliSudahAngsur($value['id']);
    }
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);
//  die;
  return $result;
 }

 public function getDataPembayaranDetail($id) {
  $data = Modules::run('database/get', array(
  'table' => 'pembeli_has_product phr',
  'field' => array('phr.*', 'r.product as nama_product',
  'p.nama as nama_pembeli', 'p.no_hp', 'p.alamat',
  'sp.status', 'rhhjc.harga as harga_cash',
  'rhhjk.harga as harga_kredit',
  'pha.id as pembeli_punya_angsuran',
  'rhha.harga as harga_angsuran',
  'a.periode_tahun', 'a.ansuran as angsuran_time',
  'pr.id as pembayaran_product_id',
  'rhha.harga_total'),
  'join' => array(
  array('pembeli p', 'phr.pembeli = p.id'),
  array('product r', 'phr.product = r.id'),
  array('status_pembelian sp', 'phr.status_pembelian = sp.id'),
  array('product_has_harga_jual_tunai rhhjk', 'rhhjk.product = r.id and rhhjk.period_end is null'),
  array('product_has_harga_jual_pokok rhhjc', 'rhhjc.product = r.id and rhhjc.period_end is null'),
  array('pembeli_has_angsuran pha', 'phr.id = pha.pembeli_has_product', 'left'),
  array('product_has_harga_angsuran rhha', 'pha.product_has_harga_angsuran = rhha.id and rhha.period_end is null', 'left'),
  array('ansuran a', 'rhha.ansuran = a.id', 'left'),
  array('pembayaran_product pr', 'pr.pembeli_has_product = phr.id', 'left'),
  ),
  'where' => array('pr.id' => $id),
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['sudah_angsur'] = '';
    if (trim(strtolower($value['status'])) == 'kredit') {
     $value['sudah_angsur'] = $this->getTotalPembeliSudahAngsur($value['id']);
     $value['detail'] = $this->getDetailAngsuran($value['pembayaran_product_id']);
    } else {
     $value['detail'] = $this->getDetailCash($value['pembayaran_product_id']);
    }
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailCash($pembelian_product) {
  $data = Modules::run('database/get', array(
  'table' => 'pembayaran_has_cash',
  'where' => array('pembayaran_product' => $pembelian_product)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDetailAngsuran($pembelian_product) {
  $data = Modules::run('database/get', array(
  'table' => 'pembayaran_has_angsuran',
  'where' => array('pembayaran_product' => $pembelian_product)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tgl_bayar'] = $value['tgl_angsuran'];
    $value['invoice'] = $value['no_invoice'];
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDetailDataPembayaranByInvoice($invoice) {
  $data = Modules::run('database/get', array(
  'table' => 'pembayaran_has_angsuran',
  'where' => array('no_invoice' => $invoice)
  ));

  $is_angusran = false;
  $result = array();
  if (!empty($data)) {
   $is_angusran = true;
   foreach ($data->result_array() as $value) {
    $value['tgl_bayar'] = $value['tgl_angsuran'];
    $value['invoice'] = $value['no_invoice'];
    array_push($result, $value);
   }
  } else {
   $data = Modules::run('database/get', array(
   'table' => 'pembayaran_has_cash',
   'where' => array('no_invoice' => $invoice)
   ));

   if (!empty($data)) {
    foreach ($data->result_array() as $value) {
     $value['invoice'] = $value['no_invoice'];
     array_push($result, $value);
    }
   }
  }


  return array(
  'data' => $result,
  'is_angsuran' => $is_angusran
  );
 }

 public function getTotalPembeliSudahAngsur($id) {
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' pr',
  'join' => array(
  array('pembayaran_has_angsuran pha', 'pha.' . $this->getTableName() . ' = pr.id')
  ),
  'where' => array('pr.pembeli_has_product' => $id)
  ));

  return $total;
 }

 public function getInvocePembelian() {
  $no_ivoice = $this->input->post('no_invoice');
  $data['content'] = $this->getDataPembelian($no_ivoice);
  echo $this->load->view('data_pembelian', $data, true);
 }

}
