<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <!--     <div class="col-md-4">
           <input type="text" id="tanggal" class="form-control" readonly>
          </div>-->
					<div class="col-md-8">
      <!--      <button id="tampil" class="btn btn-primary" onclick="LapStock.tampilkan(this)">Tampilkan</button>
            &nbsp;-->
      <a class="btn btn-success" download="<?php echo 'Laporan Pelanggan' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_laporan', 'Laporan Pelanggan');">Export</a>
					</div>
    </div>
    <br/>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive' id="data_detail">
       <table class="table table-bordered" id="tb_laporan">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>No</th>
          <th>Nama</th>
          <th>Kelurahan</th>
          <th>Kecamatan</th>
          <th>Kota</th>
          <th>Alamat</th>
          <th>No HP</th>
          <th>Email</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($data_pelanggan)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($data_pelanggan as $value) { ?>
           <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $value['nama'] ?></td>
            <td><?php echo $value['kelurahan'] ?></td>
            <td><?php echo $value['kecamatan'] ?></td>
            <td><?php echo $value['kota'] ?></td>
            <td><?php echo $value['alamat'] ?></td>
            <td><?php echo $value['no_hp'] ?></td>
            <td><?php echo $value['email'] ?></td>
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td colspan="10" class="text-center">Tidak ada data ditemukan</td>
          </tr>
         <?php } ?>

        </tbody>
       </table>
      </div>
     </div>          
    </div>        
   </div>

  </div>
 </div>
</div>
