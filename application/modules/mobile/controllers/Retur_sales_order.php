<?php

class Retur_sales_order extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'Retur_sales_order';
	}

	public function getTableName()
	{
		return 'retur_order';
	}

	public function index()
	{
		echo 'Sales_order';
	}

	public function getPostDataHeader()
	{
		$data['no_retur'] = Modules::run('no_generator/generateNoFakturReturPelanggan');
		$data['order'] = $_POST["order"];
		$data['tanggal_faktur'] = date('Y-m-d');
		$data['total'] = $_POST["total"];
		$data['keterangan'] = $_POST["keterangan"];
		$data['createddate'] = date('Y-m-d');
		$data['createdby'] = $_POST['user'];
		return $data;
	}

	public function prosesSimpan()
	{
		$is_valid = "0";

		// echo '<pre>';
		// print_r($_POST);die;
		$this->db->trans_begin();
		try {
			$post_data = $this->getPostDataHeader();
			$this->db->insert($this->getTableName(), $post_data);
			$id = $this->db->insert_id();

			//invoice product item    
			$post_item['retur_order'] = $id;
			$post_item['order_product'] = $_POST['order_product'];
			$post_item['qty'] = $_POST['qty'];
			$post_item['stok_kategori'] = $_POST['stok_kategori'];
			$post_item['sub_total'] = str_replace(',', '', $_POST['total']);

			$retur_order_item = Modules::run('database/_insert', 'retur_order_item', $post_item);
			$this->db->trans_commit();
			$is_valid = "1";
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function getListKategoriStok()
	{
		$data = Modules::run('database/get', array(
			'table' => 'stok_kategori',
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		echo json_encode(array(
			'data'=> $result
		));
	}
}
