<?php

class Helper extends MX_Controller{
 public function getIndoDate($date, $choice = true) {
  $date = date('Y-m-d', strtotime($date));
  
  list($year, $month, $day) = explode('-', $date);  
  $bulan = "";
  switch ($month) {
   case '01': 
    $bulan = "Januari";
    break;
   case '02': 
    $bulan = "Februari";
    break;
   case '03': 
    $bulan = "Maret";
    break;
   case '04': 
    $bulan = "April";
    break;
   case '05': 
    $bulan = "Mei";
    break;
   case '06': 
    $bulan = "Juni";
    break;
   case '07': 
    $bulan = "Juli";
    break;
   case '08': 
    $bulan = "Agustus";
    break;
   case '09': 
    $bulan = "September";
    break;
   case '10': 
    $bulan = "Oktober";
    break;
   case '11': 
    $bulan = "November";
    break;
   case '12': 
    $bulan = "Desember";
    break;
   default:
    break;
  }
  
  if(!$choice){
   return $bulan.' '.$year;
  }
  return $day.' '.$bulan.' '.$year;
 }

 public function getIndoDateMonth($month) {
  $bulan = "";
  switch ($month) {
   case '01': 
    $bulan = "Januari";
    break;
   case '02': 
    $bulan = "Februari";
    break;
   case '03': 
    $bulan = "Maret";
    break;
   case '04': 
    $bulan = "April";
    break;
   case '05': 
    $bulan = "Mei";
    break;
   case '06': 
    $bulan = "Juni";
    break;
   case '07': 
    $bulan = "Juli";
    break;
   case '08': 
    $bulan = "Agustus";
    break;
   case '09': 
    $bulan = "September";
    break;
   case '10': 
    $bulan = "Oktober";
    break;
   case '11': 
    $bulan = "November";
    break;
   case '12': 
    $bulan = "Desember";
    break;
   default:
    break;
  }
  
  return $bulan;
 }
}
