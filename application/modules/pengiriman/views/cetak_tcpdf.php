<!DOCTYPE html>
<html lang="en" dir="ltr">
 <body>  
  <table class="table-logo">
   <tbody>
    <tr>
     <td style="">
      <table>
       <tr>
        <td width="40"><img src="<?php echo $logo ?>" alt="" width="30"></td> 
        <td>
         <div class="font-bold" style="font-size: 9px;">Permintaan Barang<br/><?php echo $self['title'] ?><br/><?php echo $self['alamat'] ?></div>
        </td>
       </tr>
      </table>
     </td>
     <td style="">
      <table>
       <tr>
        <td class="text-right">
         <div class="font-bold" style="font-size: 9px;">ID Dokumen</div>
        </td>
        <td>
         <div style="font-size: 9px;"><?php echo $sj['no_pengiriman'] ?></div>
        </td>
       </tr>
       <tr>
        <td class="text-right">
         <div class="font-bold" style="font-size: 9px;">Tgl. Pengiriman</div>
        </td>
        <td>
         <div style="font-size: 9px;"><?php echo date("d F Y", strtotime($sj['tanggal_pengiriman'])) ?></div>
        </td>
       </tr>
       <tr>
        <td class="text-right">
         <div class="font-bold" style="font-size: 9px;">Tanggal Cetak</div>
        </td>
        <td>
         <div style="font-size: 9px;"><?php echo date('d F Y') ?></div>
        </td>
       </tr>
       <tr>
        <td class="text-right">
         <div class="font-bold" style="font-size: 9px;">Sopir</div>
        </td>
        <td>
         <div style="font-size: 9px;"><?php echo $sj['sopir'] ?></div>
        </td>
       </tr>
       <tr>
        <td class="text-right">
         <div class="font-bold" style="font-size: 9px;">Nopol</div>
        </td>
        <td>
         <div style="font-size: 9px;"><?php echo $sj['no_polisi'] ?></div>
        </td>
       </tr>
      </table>
     </td>
    </tr>
   </tbody>
  </table>
    
  <table class="table-item">
   <thead>
    <tr>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;font-size: 8px;">No</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;font-size: 8px;">Kode Produk</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;font-size: 8px;">Produk</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;font-size: 8px;">Jumlah</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;font-size: 8px;">Satuan</th>
     <!--<th>Harga</th>-->
     <!--<th>Sub Total</th>-->
    </tr>
   </thead>
   <tbody>
    <?php if (!empty($invoice_item['data'])) { ?>
     <?php $no = 1; ?>
     <?php foreach ($invoice_item['data'] as $value) { ?>
      <tr>
       <td style="font-size: 8px;border-bottom: none;border-left: none;border-right: none;border-top: none;"><?php echo $no++ ?></td>
       <td style="font-size: 8px;border-bottom: none;border-left: none;border-right: none;border-top: none;"><?php echo $value['kode_product'] ?></td>
       <td style="font-size: 8px;border-bottom: none;border-left: none;border-right: none;border-top: none;"><?php echo $value['nama_product'] ?></td>
       <td style="font-size: 8px;border-bottom: none;border-left: none;border-right: none;border-top: none;" class="text-center"><?php echo $value['qty_all'] ?></td>
       <td style="font-size: 8px;border-bottom: none;border-left: none;border-right: none;border-top: none;" class="text-center"><?php echo $value['satuan_all'] ?></td>
      </tr>
     <?php } ?>
    <?php } ?>
<!--    <tr>
 <td style="border: 0" class="text-right" colspan="4">Total</td>
 <td class="text-right font-bold"><?php echo 'Rp. ' . number_format($sj['total']) ?></td>
</tr>-->
   </tbody>
  </table>
  <br/>
  <table style="width: 100%;">
   <tbody>
    <tr style="padding-left: 12px;">
     <td style="font-size: 8px;text-align:center;">Dibuat,</td>
     <td style="font-size: 8px;text-align:center;">Diperiksa,</td>
     <td style="font-size: 8px;text-align:center;">Gudang,</td>
     <td style="font-size: 8px;text-align:center;">Sopir,</td>
     <td style="font-size: 8px;text-align:center;">Diterima oleh,</td>
     <td style="font-size: 8px;text-align:center;">&nbsp;</td>
    </tr>
    <tr>
     <td style="height: 30px"></td>
    </tr>
    <tr>
     <td class="text-center" style="font-size: 8px;">(------------------------------)</td>
     <td class="text-center" style="font-size: 8px;">(------------------------------)</td>
     <td class="text-center" style="font-size: 8px;">(------------------------------)</td>
     <td class="text-center" style="font-size: 8px;">(------------------------------)</td>
     <td class="text-center" style="font-size: 8px;">(------------------------------)</td>
    </tr>
   </tbody>
  </table>  
 </body>
</html>
