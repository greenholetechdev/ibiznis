<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Parent
     </div>
     <div class='col-md-3'>
      <?php echo $nama_parent ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Kode Akun
     </div>
     <div class='col-md-3'>
      <?php echo $code ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Akun
     </div>
     <div class='col-md-3'>
      <?php echo $keterangan ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="Coa.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
